<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\MSSQL;
use PDO;
use App\Http\Controllers\Core;
use App\Http\Controllers\EmailController;
use App\Http\Controllers\StorageEngine;
use Psy\Command\WhereamiCommand;
use ZipArchive;
use Response;


class ServCheckController extends Controller
{
    private $department_id = '';

    public static function ListOfDepartments()
    { //список всех органов власти
        if (DB::connection()->getDatabaseName()) {
            $sql = "SELECT id, name FROM `departments` order by name";
            $array_result = DB::select($sql);
            return json_encode($array_result, JSON_UNESCAPED_UNICODE);
        } else {
            exit('{"type":"error", "error":"Нет соединения с базой данных"}');
        };
    }

    public static function ListOfActions() //виды действий в сервисе
    { //список всех действий в сервисе
        $array_result = array(
            1  => 'Создать новый АКТ',
            2 => 'Редактировать АКТ',
            3 => 'Открыть АКТ'
        );
        return json_encode($array_result, JSON_UNESCAPED_UNICODE);
    }

    /*public static function FindMember($letter = '', $kind = 'front')
    { //список всех сотрудников МФЦ для выбора членов комиссии
        $conn = MSSQL::connect();

        $sql = "SELECT
                [pref_user].[id]
                ,[display_name]
                ,[username]
                ,[first_name]
                ,[last_name]
                ,[patronymic]
                ,[active_directory_login]
               ,[position]
                FROM [TEST_ATConsalting].[dbo].[pref_user]
                where [display_name] not like '%[a-z]%' or [display_name] not like '%[A-Z]%'
                --where last_name like '{$letter}%'
                order by last_name
                ";

        if ($kind == 'front') {
            $sql = "SELECT
                [pref_user].[id]
                ,[display_name]
                FROM [TEST_ATConsalting].[dbo].[pref_user]
                where [display_name] not like '%[a-z]%' or [display_name] not like '%[A-Z]%'
                order by last_name
                ";
        };
        $array_result = MSSQL::select($conn, $sql);
        if (count($array_result) != 0) {
            return json_encode($array_result, JSON_UNESCAPED_UNICODE);
        };
    }*/

    public static function FindMember($letter = '', $kind = 'front')
    { //список всех сотрудников МФЦ для выбора членов комиссии
        $conn = MSSQL::connect();

        $sql = "SELECT
                [pref_user].[id]
                ,[display_name]
                ,[username]
                ,[first_name]
                ,[last_name]
                ,[patronymic]
                ,[active_directory_login]
               ,[position]
                FROM [TEST_ATConsalting].[dbo].[pref_user]
                where [display_name] not like '%[a-z]%' or [display_name] not like '%[A-Z]%'
                --where last_name like '{$letter}%'
                order by last_name
                ";

        if ($letter != '') {
            $query = " and [display_name] like '{$letter}%'";
        } else {
            $query = '';
        };
        if ($kind == 'front') {
            $sql = "SELECT
                [pref_user].[id]
                ,[display_name]
                ,[position]
                FROM [TEST_ATConsalting].[dbo].[pref_user]
                where ([display_name] not like '%[a-z]%' or [display_name] not like '%[A-Z]%')" . $query . "
                order by last_name";
        };
        $array_result = MSSQL::select($conn, $sql);
        if (count($array_result) != 0) {
            return json_encode($array_result, JSON_UNESCAPED_UNICODE);
        };
    }

    public static function FindServiceMember($kind = 'front')
    { //Список всех сотрудников из таблицы users-info
        if (DB::connection()->getDatabaseName()) {
            $sql = "SELECT `id`,`firstname`,`name`,`surname`,`title`,`department`,`email`, uuid FROM `users-info` WHERE `uuid` != '48a8cd01-4449-4895-b2e4-b1c973e3e030' order by firstname, name, surname";
            $array_result = DB::select($sql);
            if (count($array_result) > 0) {
                foreach ($array_result as $value) {
                    $temp = array();
                    $temp['id'] = $value->id;
                    $temp['display_name'] = $value->firstname . ' ' . $value->name . ' ' . $value->surname;
                    $temp['title'] = $value->title; //должность
                    $temp['department'] = $value->department;
                    $temp['uuid'] = $value->uuid;

                    if ($kind == 'back') {
                        $temp['email'] = $value->email;
                    };
                    $member[] = $temp;
                };
                return json_encode($member, JSON_UNESCAPED_UNICODE);
                //return $member;
            }
        } else {
            exit('{"type":"error", "error":"Нет соединения с базой данных"}');
        };
    }

    public static function GetPosition($pref_user)
    { //должность сотрудника
        $conn = MSSQL::connect();
        $sql = "SELECT
                [display_name],
                [position]
                FROM [TEST_ATConsalting].[dbo].[pref_user]
                where [pref_user].[id] = {$pref_user}";
        $array_result = MSSQL::select($conn, $sql);

        //$array_result[0]->position;$array_result[0]->user_fio_name
        //$result =
        $array_result = (array) $array_result[0];
        return $array_result;
    }

    public static function GetEmployeePosition($pref_user)
    { //должность сотрудника из таблицы users-info
        if (DB::connection()->getDatabaseName()) {
            $sql = "SELECT Concat(`firstname`,' ',`name`,' ',`surname`) as display_name,`title`as position FROM `users-info` where id = {$pref_user}";
            $array_result = DB::select($sql);
            //$array_result = (array) $array_result[0];
            //return $array_result;
            return json_encode($array_result, JSON_UNESCAPED_UNICODE);
        }
    }

    public static function GetComment($order_number, $order_id)
    { //получаем комментарии приемщика, о том что он предупреждал заявителя TODO:решить вопрос с роботом информатором

        /*TODO:
        [dbo].[pref_order_large_fields]
        "NoteInformation"
        */

        $conn = MSSQL::connect();
        $array_result = array();
        $sql = "
            SELECT
                [order_status_history].[comment]
                ,[order_status_history].[timestamp]
            from
                [TEST_ATConsalting].[dbo].[pref_order],
                [TEST_ATConsalting].[dbo].[pref_user],
                [TEST_ATConsalting].[dbo].[order_status_history]
            where
                order_status_history.pref_user = pref_user.id
                and order_status_history.pref_order = [pref_order].id
                and [pref_order].order_number = '{$order_number}'
                and [order_status_history].comment is not null and comment not in ('', ' ')
                and  order_status_history.pref_user is not null ";

        $array_result = MSSQL::select($conn, $sql);
        //$res = [];
        foreach ($array_result as $key => $value) {
            //$temp = array();
            //$temp['id'] = $key;
            //$temp['comment'] = strip_tags($value['comment']);
            //$temp['timestamp'] = date("d.m.Y H:i:s", strtotime($value['timestamp']));
            // $res[$key] = $temp;

            $comment = strip_tags(addslashes($value['comment']));
            //$timestamp = date("d.m.Y H:i:s", strtotime($value['timestamp']));
            $timestamp = $value['timestamp'];
            $sql = "INSERT INTO `pref_order_comment`(`order_id`, `comment`, `timestamp`) VALUES ({$order_id},'{$comment}','{$timestamp}')";
            DB::insert($sql);
            self::DBLog($sql);
        };

        //return json_encode($res, JSON_UNESCAPED_UNICODE);
        //return $res;
    }

    public static function FindComments($order_id)
    {
        $sql = "SELECT `id`, `comment`, `timestamp` FROM `pref_order_comment` WHERE `order_id` = {$order_id} order by `timestamp` desc ";
        $array_result = DB::select($sql);

        $res = [];
        foreach ($array_result as $key => $value) {
            $temp = array();
            $temp['id'] = $key;
            $temp['comment'] = strip_tags($value->comment);
            $temp['timestamp'] = date("d.m.Y H:i:s", strtotime($value->timestamp));
            $res[$key] = $temp;
        };
        return $res;
        //return json_encode($res, JSON_UNESCAPED_UNICODE);
    }

    public static function GetDataFromMSSQL($order_number, $kind = 'front')
    { //получить все данные по номеру дела из MSSQL
        $conn = MSSQL::connect();

        if ($kind == 'back') {
            $full_sql = "
                  ,[pref_order].[service_eid]
                  ,[pref_order].[department_id]
                  ,[pref_department].[nick]
                  ,[pref_order].[mfc]
                  ,[pref_order].[pref_user]
                  ,[pref_order].[reception_place_value]
            ";
        } else {
            $full_sql = '';
        };

        $sql = "
            SELECT
                [pref_order].[id]
                ,[pref_order].[order_number]
                ,[pref_order].[order_date]
                ,[pref_order].[service_title]
                ,[pref_user].display_name
                , [pref_order].pk_pvd_number
                ,pref_mfc.name" . $full_sql . "
            FROM [TEST_ATConsalting].[dbo].[pref_order], [TEST_ATConsalting].[dbo].[pref_department], [TEST_ATConsalting].[dbo].[pref_mfc], [TEST_ATConsalting].[dbo].[pref_user]
            where [pref_order].deleted = 0  AND
            [pref_order].order_type = 'SERVICE' and
            [pref_order].department_id = [pref_department].id and
            [pref_order].mfc = [pref_mfc].id and
            [pref_order].pref_user = [pref_user].id and
            [pref_order].order_number = '" . $order_number . "'";

        //echo $sql;

        $array_result = MSSQL::select($conn, $sql);
        if (count($array_result) == 1) {

            return $array_result;
        } else {
            exit('{"type":"error", "error":"Дело не найдено"}');
        }
    }

    public static function OrderDuplicate($order_number, $act_id)
    { //проверка не добавлено ли дело дважды в акт
        if (DB::connection()->getDatabaseName()) {
            $sql = "SELECT count(*) FROM `servcheck` WHERE order_number = '" . $order_number . "' and act_id = {$act_id}";
            //TODO: сделать проще следующие две строки
            $array_result = DB::select($sql);
            $array_result = (array) $array_result[0];

            if ($array_result['count(*)'] == 0) {
                return true; //нет дубликатов
            } else {
                return false;
            }; //такое дело уже есть
        }
    }

    public static function GetActs($department_id = '',  $month = '', $year = '', $status = '')
    { //получаем все данные по всем актам с преминением фильтров, отсортированные по статусу и дате
        //сначала статус Новый, потом В работе, потом Закрыт
        if (DB::connection()->getDatabaseName()) {
            $sql = "SELECT acts.id as id, serial_number, DAY(act_date) as day, MONTH(act_date) as month, YEAR(act_date) as year, acts.department_id as department_id, name as department_name, IFNULL(status, 'Новый') as status_name, status, CASE WHEN status = 1 THEN 2 WHEN status = 0 THEN 3 WHEN status is null THEN 1 END AS sort_status FROM acts, departments WHERE acts.department_id = departments.id ";
            if ($department_id != '') {
                $sql = $sql . " and department_id = {$department_id}";
            };
            if ($month != '') {
                $sql = $sql . " and MONTH(`act_date`) = {$month}";
            };
            if ($year != '') {
                $sql = $sql . " and YEAR(`act_date`) = {$year}";
            };
            if ($status != '') {
                switch ($status) {
                    case 0:
                        $sql = $sql . " and status = 0";
                        break;
                    case 1:
                        $sql = $sql . " and status = 1";
                        break;
                    case 'null':
                        $sql = $sql . " and status is null";
                        break;
                };
            };

            $sql = $sql . " order by sort_status, act_date desc";
            $array_result = DB::select($sql);

            /*
            SELECT acts.id as id, serial_number, MONTH(act_date) as month, YEAR(act_date) as year, department_id, name as department_name, IFNULL(status, 'Новый') as status_name, status, CASE WHEN status = 1 THEN 2 WHEN status = 0 THEN 3 WHEN status is null THEN 1 END AS sort_status FROM acts, departments WHERE acts.department_id = departments.id order by sort_status, act_date desc;
             */

            array_walk($array_result, function (&$v) {
                if ($v->status_name == 0) $v->status_name = 'Завершен';
            });
            array_walk($array_result, function (&$v) {
                if ($v->status_name == 1) $v->status_name = 'В работе';
            });

            return $array_result;

            //return json_encode($array_result, JSON_UNESCAPED_UNICODE);
        } else {
            exit('{"type":"error", "error":"Нет соединения с базой данных"}');
        };
    }

    public static function ArrayResolutionOfMembers($act_id)
    { //указываем для каждого члена комиссии в данном акте, сколько он внес резолюций (количество)

        $ActMembersFromDB = self::GetMembersFromAct($act_id); //получаем всех членов из акта
        $arr_resolution = array(); // массив резолюций и айдишников членов комиссии

        if (!!($ActMembersFromDB['president'])) {
            $res = self::CountResolutionOfMember($act_id, $ActMembersFromDB['president'][0]);
            if (!!($res)) {                     //если массив с резолюциями членов по делам не пустой
                $array_result = (array) $res[0];
                $arr_resolution[$array_result['member_id']] = $array_result['cnt_resol'];
            } else { //председатель есть, но нет записи в таблице pref_order_member для него TODO: делать проверку - аномалия базы!! пока костыль
                $arr_resolution[$ActMembersFromDB['president'][0]] = 0;
            };
        };

        if (!!($ActMembersFromDB['secretary'])) {
            $res = self::CountResolutionOfMember($act_id, $ActMembersFromDB['secretary'][0]);
            if (!empty($res)) {
                $array_result = (array) $res[0];
                $arr_resolution[$array_result['member_id']] = $array_result['cnt_resol'];
            } else { //секретарь есть, но нет записи в таблице pref_order_member для него TODO: делать проверку - аномалия базы!! пока костыль
                $arr_resolution[$ActMembersFromDB['secretary'][0]] = 0;
            };
        };

        if (!empty($ActMembersFromDB['member'])) {
            $act_members = $ActMembersFromDB['member'];
            foreach ($act_members as $value) {
                $res = self::CountResolutionOfMember($act_id, $value);
                if (!empty($res)) {
                    $array_result = (array) $res[0];
                    $arr_resolution[$array_result['member_id']] = $array_result['cnt_resol'];
                } else { //член комиссии есть, но нет записи в таблице pref_order_member для него TODO: делать проверку - аномалия базы!! пока костыль
                    $arr_resolution[$value] = 0;
                };
            };
        };

        return $arr_resolution;
    }

    public static function GetActInfo($act_id)
    {
        if (DB::connection()->getDatabaseName()) {
            //$sql = "SELECT acts.`id`,`serial_number`,`act_date`, acts.`department_id`,`prikaz_id`,`prikaz_date`,`prikaz_name`,`members`,`status`, name, count(servcheck.id) as count_order FROM `acts`, departments, servcheck WHERE acts.`id` = {$act_id} and departments.id = acts.department_id and servcheck.act_id = acts.`id` group by acts.`id`,`serial_number`,`act_date`, acts.`department_id`,`prikaz_id`,`prikaz_date`,`prikaz_name`,`members`,`status`, name";

            //найдем отдельным запросом кол-во дел в Акте для диаграммы на вкладке Комиссия

            $sql = "SELECT count(*) as count_order FROM servcheck WHERE act_id = {$act_id}";
            $res = DB::select($sql);
            $res = (array) $res[0];
            $count_order = $res['count_order']; //кол-во дел в акте
            $count_temp = count(self::GetMembersFromAct($act_id), COUNT_RECURSIVE) - 3;
            $count_order_diagram = $count_order * $count_temp;
            unset($res);

            $sql = "SELECT acts.`id`,`serial_number`,`act_date`,`department_id`,`prikaz_id`,`prikaz_date`,`prikaz_name`,`members`,`status`, name FROM `acts`, departments WHERE acts.`id` = {$act_id} and departments.id = acts.department_id";

            $array_result = DB::select($sql);
            if (count($array_result) > 0) {                     //если найден хоть один акт
                foreach ($array_result as $value) {
                    $acts['id'] = $value->id;
                    $acts['serial_number'] = $value->serial_number;
                    $acts['act_date'] = date("d.m.Y", strtotime($value->act_date));
                    $acts['serial_number'] = $value->serial_number;
                    $acts['department_id'] = $value->department_id;
                    $acts['count_order'] = $count_order; //кол-во дел в акте
                    $acts['count_order_diagram'] = $count_order_diagram; //для диаграммы
                    $acts['prikaz_id'] = $value->prikaz_id;
                    $acts['prikaz_date'] = $value->prikaz_date;
                    $acts['prikaz_name'] = $value->prikaz_name;
                    //TODO: здесь переделать или нет ???
                    $acts['members'] = self::GetMembersFromAct($act_id); //$value->members;

                    $acts['status'] = $value->status;
                    $acts['department_name'] = $value->name;
                };

                //указываем для каждого члена комиссии в данном акте, сколько он внес резолюций (количество)
                $acts['member_cnt_resolution'] = self::ArrayResolutionOfMembers($act_id);

                //$acts['member_cnt_resolution'] = $arr_resolution;
                //var_dump($arr_resolution);

                $user_data = Core::Init('guest'); //получаем id авторизованного пользователя
                $user_id = $user_data['user']['id'];
                //находим роль пользователя в данном акте
                $sql = "SELECT `role` FROM `acts_member` WHERE `pref_user` = {$user_id} and `act_id` = {$act_id}";
                $res = DB::select($sql);
                if (count($res) == 1) {
                    $role = $res[0]->role;
                } else {
                    $role = '3';
                };
                $acts['role'] = $role;

                //$acts['members'] = self::GetMembersFromAct($act_id);
                //var_dump($acts['members']);

                //$sql = "SELECT `id`,`order_date`,`order_number`,`service_title`,`department_name`,`mfc_name`,`user_fio_name`,`description`,`mistake_id`,`comment`,`employee_note`,`resolution` FROM `servcheck` WHERE `act_id` = {$act_id}";

                $sql = "SELECT `id`,`order_date`,`order_number`,`mfc_name`, pref_user, `user_fio_name`, `pk_pvd_number`,
                            CASE
                                WHEN `resolution` = 2 THEN 'виновен'
                                WHEN `resolution` = 1 THEN 'не виновен'
                                ELSE 'нет данных'
                            END as resolution
                        FROM `servcheck` WHERE `act_id` = {$act_id} order by `mfc_name`";

                $array_result_order = DB::select($sql);
                if (count($array_result_order) > 0) {

                    for ($i = 0; $i < count($array_result_order); $i++) {
                        $pref_user = $array_result_order[$i]->pref_user; //айди пользователя из MSSQL
                        $user_info = self::GetPosition($pref_user);
                        $array_result_order[$i]->position = $user_info['position'];
                        //echo $array_result_order[$i]->position;
                        $array_result_order[$i]->critical = 0;
                        $arr_mistake = self::FindOrderMistakes($array_result_order[$i]->id);
                        if (count($arr_mistake) == 0) {
                            $empty = 'новое';
                        } else {
                            $empty = 'в работе';
                            $array_result_order[$i]->critical = 0;
                            for ($j = 0; $j < count($arr_mistake); $j++) {
                                if ($arr_mistake[$j]['critical'] == 1) {
                                    $array_result_order[$i]->critical = 1;
                                };
                            };
                        };
                        $array_result_order[$i]->empty_flag = $empty;
                        $array_result_order[$i]->order_date = date("d.m.Y", strtotime($array_result_order[$i]->order_date));
                    };
                    //json_encode($array_result_order, JSON_UNESCAPED_UNICODE);
                    //$array_result_order[0]->comment_order = self::GetComment($array_result_order[0]->id);
                    //$acts['orders'] = json_encode($array_result_order, JSON_UNESCAPED_UNICODE);
                    $acts['orders'] = $array_result_order;

                    //$acts['comment'] = $comment;
                    //$acts['members'] = self::GetMembersFromAct($act_id);

                } else {
                    $acts['orders'] = '';
                };
                return json_encode($acts, JSON_UNESCAPED_UNICODE);
            }
        } else {
            exit('{"type":"error", "error":"Нет соединения с базой данных"}');
        };
    }

    public static function GetOrdersTable($act_id)
    {
        $sql = "SELECT `id`,`order_date`,`order_number`,`mfc_name`,`user_fio_name`,`resolution`, `pk_pvd_number`,
                    CASE
                        WHEN `resolution` = 2 THEN 'виновен'
                        WHEN `resolution` = 1 THEN 'не виновен'
                        ELSE 'нет данных'
                    END as resolution
                FROM `servcheck` WHERE `act_id` = {$act_id}";
        $array_result_order = DB::select($sql);
        if (count($array_result_order) > 0) {

            for ($i = 0; $i < count($array_result_order); $i++) {
                $array_result_order[$i]->critical = 0;
                $arr_mistake = self::FindOrderMistakes($array_result_order[$i]->id);
                if (count($arr_mistake) == 0) {
                    $empty = 'новое';
                } else {
                    $empty = 'в работе';
                    $array_result_order[$i]->critical = 0;
                    for ($j = 0; $j < count($arr_mistake); $j++) {
                        if ($arr_mistake[$j]['critical'] == 1) {
                            $array_result_order[$i]->critical = 1;
                        };
                    };
                };
                $array_result_order[$i]->empty_flag = $empty;
                $array_result_order[$i]->order_date = date("d.m.Y", strtotime($array_result_order[$i]->order_date));
                $acts['orders'] = $array_result_order;
            };
        } else {
            $acts['orders'] = '';
        };
        return json_encode($acts, JSON_UNESCAPED_UNICODE);
    }

    public static function GetOrderInfo($order_id)
    { //получить информацию по делу
        if (DB::connection()->getDatabaseName()) {
            $sql = "SELECT `id`,`order_date`,`order_number`,`service_title`,`mfc_name`,`user_fio_name`,`description`,`comment`,`employee_note`,`resolution`,`act_id`, pk_pvd_number, department_name FROM `servcheck` WHERE id = {$order_id}";

            $res = DB::select($sql);
            $res[0]->order_date = date('d.m.Y', strtotime($res[0]->order_date));

            $arr_comment = [];
            $arr_comment = self::FindComments($order_id);
            $arr_mistake = self::FindOrderMistakes($order_id);

            if (count($arr_mistake) == 0) {
                $empty = 'новое';
                $mistake_recommend = [];
            } else {
                $empty = 'в работе';
                //список рекоммендаций по делу-ошибке
                $mistake_recommend = array();
                foreach ($arr_mistake as $key => $value) {
                    $mistake_recommend[$value['id']] = self::GetRecommendFromOrderMistake($order_id, $value['mis_id']);
                };
            };

            $res[0]->recommend = $mistake_recommend;
            $res[0]->comment_order = $arr_comment;
            $res[0]->selected_mistakes = $arr_mistake;
            $res[0]->empty_flag = $empty;

            //получим список категорий ошибок для выбранного органа власти

            $sql = "SELECT `department_id`, serial_number FROM `acts` WHERE id = (select act_id from servcheck where id = {$order_id})";
            $department_res = DB::select($sql);
            if (count($department_res) == 1) {
                $department_id = $department_res[0]->department_id;
                $res[0]->act_number = $department_res[0]->serial_number; //добавила Даша
                $res[0]->categories = self::ListOfCategories($department_id);
            } else {
                $res[0]->categories = json_encode('', JSON_UNESCAPED_UNICODE);
            }; //если нет категорий ошибок у департамента

            return json_encode($res, JSON_UNESCAPED_UNICODE);
        };
    }

    public static function FindOrderMistakes($order_id)
    { //получить все ошибки указанные в акте
        $sql = "SELECT orders.id, mis.id as mis_id, mis.description as mis_name, cat.id as cat_id, cat.name as cat_name, critical FROM `pref_mistake` as mis, pref_category_mistake as cat, `pref_order_mistake` as orders WHERE cat.id = mis.category_id and orders.mistake_id = mis.id and orders.order_id = {$order_id}";
        $array_result = DB::select($sql);

        $res = [];
        foreach ($array_result as $key => $value) {
            $temp = array();
            //$temp['id'] = $key;
            $temp['id'] = $value->id;
            $temp['mis_id'] = $value->mis_id;
            $temp['mis_name'] = $value->mis_name;
            $temp['cat_id'] = $value->cat_id;
            $temp['cat_name'] = $value->cat_name;
            $temp['critical'] = $value->critical;

            $res[$key] = $temp;
        };
        return $res;
    }

    public static function GetMembersFromAct($act_id)
    {
        $sql = "SELECT `id`,`pref_user`,`role` FROM `acts_member` WHERE act_id = {$act_id}";
        $res = DB::select($sql);
        $role1 = [];
        $role2 = [];
        $role0 = [];
        $roles = array('president' => '', 'secretary' => '', 'member' => '');
        //$firstquarter = array(1 => 'January', 'February', 'March');
        foreach ($res as $value) {
            switch ($value->role) {
                case 1:
                    $role1[] = $value->pref_user;
                    break;
                case 2:
                    $role2[] = $value->pref_user;
                    break;
                case 0:
                    $role0[] = $value->pref_user;
                    break;
            }
        };
        $roles['president'] = $role1;
        $roles['secretary'] = $role2;
        $roles['member'] = $role0;
        return $roles;
    }

    public static function DropAct($act_id, $kind = 'newact')
    { //удаление Акта и всех записей из таблицы pref_members и servcheck
        if (DB::connection()->getDatabaseName()) {
            //если задан параметр ПолныйАкт, то сначала удаляем дела из этого Акта
            if ($kind == 'fullact') {
                $sql = "Select id from servcheck where act_id = {$act_id}";
                $res = DB::select($sql);
                if (count($res) > 0) {
                    foreach ($res as $value) {
                        self::DeleteOrder($value->id);
                    };
                };
            };
            //Акт - новый, в нем нет дел
            $sql = "delete from acts_member WHERE act_id ={$act_id}";
            DB::delete($sql);
            self::DBLog($sql);
            $sql = "delete from acts WHERE id ={$act_id}";
            if (DB::delete($sql)) {
                self::DBLog($sql);
                return json_encode('', JSON_UNESCAPED_UNICODE);
            };
        } else {
            exit('{"type":"error", "error":"Нет соединения с базой данных"}');
        };
    }

    public static function PutDataToMySQL($data_array)
    {
        if (DB::connection()->getDatabaseName()) { //Проверяем подключение к базе
            //проверить БД на дубль
            //$array_result = self::GetDataFromMSSQL($data_array[0]['order_number']);
            if (self::OrderDuplicate($data_array[0]['order_number'], $data_array[0]['act_id'])) {
                $sql = "Insert into servcheck (
                order_date,
                order_number,
                service_eid,
                service_title,
                department_id,
                department_name,
                mfc_id,
                mfc_name,
                pref_user,
                user_fio_name,

                description,
                pk_pvd_number,
                comment,
                employee_note,
                resolution,
                act_id) values (
                '" . $data_array[0]['order_date'] . "',
                '" . $data_array[0]['order_number'] . "',
                '" . $data_array[0]['service_eid'] . "',
                '" . $data_array[0]['service_title'] . "',
                " . $data_array[0]['department_id'] . ",
                '" . $data_array[0]['nick'] . "',
                " . $data_array[0]['mfc'] . ",
                '" . $data_array[0]['name'] . "',
                " . $data_array[0]['pref_user'] . ",
                '" . $data_array[0]['display_name'] . "',

                '" . $data_array[0]['description'] . "',
                '" . $data_array[0]['pk_pvd_number'] . "',
                '" . $data_array[0]['comment'] . "',
                '" . $data_array[0]['employee_note'] . "',
                " . 0 . ",
                " . $data_array[0]['act_id'] . ")";

                //. $data_array[0]['mistake_id'] .
                //. $data_array[0]['resolution'] .
                $id = 0;
                if (DB::insert($sql)) {
                    $order_id = DB::getPdo()->lastInsertId();
                    self::DBLog($sql);
                    /*$sql = "SELECT max(id) as id FROM `servcheck`";
                    $array_result = DB::select($sql);
                    $id = $array_result[0]->id;*/
                    $id = $order_id;
                    $sql = "SELECT pref_user, role FROM `acts_member` WHERE act_id = {$data_array[0]['act_id']}";
                    $res = DB::select($sql);

                    if (count($res) > 0) {
                        foreach ($res as $val) {
                            self::AddOrderMember($order_id, $val->pref_user, $val->role);
                        };
                    };
                };
                return $id; //вернем либо 0, либо id вновь-добавленного дела
            } else {
                exit('{"type":"error", "error":"Дело уже есть в Акте"}');
            };
        } else {
            exit('{"type":"error", "error":"Нет соединения с базой данных"}');
        };
    }

    public static function AddMistakeToOrder($order_id, $mistake_id, $critical = 0)
    { //добавляем новую ошибку и маркер критической ошибки в таблицу pref_order_mistake
        if (DB::connection()->getDatabaseName()) {
            //$sql = "SELECT pref_order_mistake.`order_id`, pref_order_mistake.`mistake_id`,`critical` FROM pref_order_mistake, servcheck WHERE pref_order_mistake.mistake_id = {$mistake_id} and pref_order_mistake.order_id = {$order_id} and servcheck.act_id = {$act_id} and pref_order_mistake.`order_id`= servcheck.id";
            //$array_result = DB::select($sql);

            $sql = "INSERT INTO `pref_order_mistake`(`order_id`, `mistake_id`, `critical`) VALUES ({$order_id}, {$mistake_id}, {$critical})";
            return DB::insert($sql);
        } else {
            exit('{"type":"error", "error":"Нет соединения с базой данных"}');
        };
    }

    public static function ListOfCategories($department_id)   // получить список видов ошибок для определенного Органа власти
    {
        if (DB::connection()->getDatabaseName()) {
            $sql = "SELECT * FROM pref_category_mistake WHERE pref_category_mistake.department_id = " . $department_id;
            $array_result = DB::select($sql);
            if (count($array_result) != 0) {
                foreach ($array_result as $value) {
                    $temp = array();
                    $temp['id'] = $value->id;
                    $temp['name'] = $value->name;
                    //$categories[$temp['id']] = $temp;
                    $categories[] = $temp;
                };
            } else {
                $categories = array();
            };
            //return $categories;
            return json_encode($categories, JSON_UNESCAPED_UNICODE);
        } else {
            exit('{"type":"error", "error":"Нет соединения с базой данных"}');
        }
    }

    public static function ListOfMistakes($category_id) // получить список ошибок для определенного Органа власти и вида ошибки
    {
        if (DB::connection()->getDatabaseName()) {
            $sql = "SELECT pref_mistake.id as id, pref_mistake.`category_id` as category_id, pref_mistake.`description` as description FROM `pref_mistake`, pref_category_mistake WHERE pref_mistake.category_id = " . $category_id . " and pref_mistake.category_id = pref_category_mistake.id";
            $array_result = DB::select($sql);
            if (count($array_result) != 0) {

                foreach ($array_result as $value) {
                    $temp = array();
                    $temp['id'] = $value->id;
                    //$temp['category_id'] = $value->category_id;
                    $temp['description'] = $value->description;
                    $mistakes[$temp['id']] = $temp;
                };
            } else {
                $mistakes = array();
            };
            //return $mistakes;
            return json_encode($mistakes, JSON_UNESCAPED_UNICODE);
        } else {
            exit('{"type":"error", "error":"Нет соединения с базой данных"}');
        };
    }

    public static function FindHeadDataAct($act_id) //заголовок АКТа
    {
        if (DB::connection()->getDatabaseName()) {
            $sql = "SELECT `acts`.`id`,`serial_number`,`act_date`,`prikaz_id`, pref_prikaz.prikaz_date as prikaz_date, pref_prikaz.prikaz_name as prikaz_name, `members` FROM `acts`, pref_prikaz WHERE acts.prikaz_id = pref_prikaz.id and acts.id = {$act_id}";
            $array_result = DB::select($sql);
            if (count($array_result) == 1) {
                foreach ($array_result as $value) {
                    $HeadDataAct = array();
                    $HeadDataAct['id'] = $value->id;
                    $HeadDataAct['serial_number'] = $value->serial_number;
                    $HeadDataAct['act_date'] = $value->act_date;
                    $HeadDataAct['prikaz_id'] = $value->prikaz_id;
                    $HeadDataAct['prikaz_date'] = $value->prikaz_date;
                    $HeadDataAct['prikaz_name'] = $value->prikaz_name;
                    $HeadDataAct['members'] = $value->members;
                };
                return $HeadDataAct;
            };
            //return json_encode($array_result, JSON_UNESCAPED_UNICODE);
        } else {
            exit('{"type":"error", "error":"Нет соединения с базой данных"}');
        };
    }

    public static function FindTableDataAct($act_id) //найти табличные данные конкретного Акта
    {
        if (DB::connection()->getDatabaseName()) {
            $sql = "SELECT acts.id as acts_id, serial_number, act_date, prikaz_id, prikaz_date, prikaz_name, members, ServCheck.id as ServCheck_id, `order_date`, `order_number`, `service_eid`, `service_title`, `department_id`, `department_name`, `mfc_id`, `mfc_name`, `pref_user`, `user_fio_name`, ServCheck.`description` as ServCheck_description, `mistake_id`, `comment`, `employee_note`, `resolution`, pref_mistake.`description` as pref_mistake_description FROM `acts`, ServCheck, pref_mistake WHERE ServCheck.act_id = acts.id and pref_mistake.id = ServCheck.mistake_id and ServCheck.act_id = " . $act_id;

            $array_result = DB::select($sql);
            if (count($array_result) != 0) {
                foreach ($array_result as $value) {
                    $temp = array();
                    $temp['id'] = $value->ServCheck_id;
                    $temp['acts_id'] = $value->acts_id;
                    $temp['serial_number'] = $value->serial_number;
                    $temp['act_date'] = $value->act_date;
                    $temp['prikaz_id'] = $value->prikaz_id;
                    $temp['prikaz_date'] = $value->prikaz_date;
                    $temp['prikaz_name'] = $value->prikaz_name;
                    $temp['members'] = $value->members;
                    $temp['order_number'] = $value->order_number;
                    $temp['mfc_name'] = $value->mfc_name;
                    $temp['user_fio_name'] = $value->user_fio_name;
                    $temp['comment'] = $value->comment;
                    $temp['employee_note'] = $value->employee_note;
                    $temp['resolution'] = $value->resolution;
                    $temp['pref_mistake_description'] = $value->pref_mistake_description;
                    $TableDataAct[$temp['id']] = $temp;
                };
                return $TableDataAct;
            } else {
                exit('{"type":"error", "error":"Табличная часть Акта не заполнена"}');
            };
        } else {
            exit('{"type":"error", "error":"Нет соединения с базой данных"}');
        };
    }

    public static function AddNewAct($head_data_act)
    {
        if (DB::connection()->getDatabaseName()) { //Проверяем подключение к базе
            $sql = "SELECT max(`serial_number`) as act_number, year(`act_date`) as act_year FROM `acts` group by act_year order by act_year desc limit 1";
            $array_result = DB::select($sql);
            if (count($array_result) > 0) {
                $serial_number = $array_result[0]->act_number; //серийный номер Акта
                $last_act_year = $array_result[0]->act_year; //год последнего созданного Акта
            } else {
                $serial_number = 0;
                $last_act_year = date("Y");
            };


            $current_year = date("Y");

            if ($last_act_year < $current_year) {
                $serial_number = 1;
            } else {
                $serial_number = $serial_number + 1;
            };

            $act_date = date("Y-m-d H:i:s");
            $prikaz_id = 1;                         //TODO: не забыть изменить на данные с фронта
            $prikaz_date = date("Y-m-d H:i:s");
            $prikaz_name = 'О назначении служебной проверки';

            if (array_key_exists('members', $head_data_act)) {
                if (strlen($head_data_act['members']) > 0) {
                    $members = $head_data_act['members']; //члены комиссии в виде строки
                    $sql_members = "'{$members}'";
                } else {
                    $sql_members = 'NULL';
                };
            } else {
                $sql_members = 'NULL';
            };

            $department_id = $head_data_act['department_id'];
            //$status = 1; //в работе

            try { //TODO: проверить try catch
                $sql = "Insert into acts (`serial_number`,`act_date`,`prikaz_id`,`prikaz_date`,`prikaz_name`,`members`,`department_id`, `status`) VALUES ({$serial_number}, '{$act_date}', {$prikaz_id}, '{$prikaz_date}', '{$prikaz_name}', " . $sql_members . ", {$department_id}, NULL)";

                DB::insert($sql);
                self::DBLog($sql);

                $sql = "Select max(id) as id from acts";
                $array_result = DB::select($sql);
                $act_id = $array_result[0]->id; //номер нового Акта

                if (array_key_exists('arr_members', $head_data_act)) {
                    self::AddMembersToAct($act_id, $head_data_act);
                };
                return $act_id;
            } catch (Exception $e) {
                echo 'Исключение: ',  $e->getMessage(), "\n";
            };
        } else {
            exit('{"type":"error", "error":"Нет соединения с базой данных"}');
        };
    }

    public static function AddMembersToAct($act_id, $head_data_act)
    {
        $array_members = $head_data_act['arr_members']; //члены комиссии в виде массива
        if (array_key_exists('arr_roles', $head_data_act)) {
            $arr_roles = $head_data_act['arr_roles'];
        };
        $data_member = array();
        foreach ($array_members as $key => $value) {
            //$data_member = self::GetPosition($value);//получить должность по id_user из MSSQL
            //$user_fio_name = $data_member['display_name'];
            //$position = $data_member['position'];

            //$data_member = self::GetEmployeePosition($value); //получить должность сотрудника по id_user из MySQL
            $data_member = json_decode(self::GetEmployeePosition($value), true);
            $user_fio_name = $data_member[0]['display_name'];
            $position = $data_member[0]['position'];

            /*switch ($key) {
                case 0:
                    $role = 1;
                    break;
                case count($array_members) - 1:
                    $role = 2;
                    break;
                default:
                    $role = 0;
            };*/

            if (!isset($arr_roles[$key])) {
                $role = 0;
            } else {
                $role = $arr_roles[$key];
            };

            $sql = "Insert into acts_member (`act_id`,`pref_user`,`user_fio_name`,`position`,`role`) values ({$act_id}, {$value}, '{$user_fio_name}', '{$position}',{$role})";
            //echo $sql;
            DB::insert($sql);
            $member_id = DB::getPdo()->lastInsertId();
            self::DBLog($sql);

            $sql = "SELECT `id` FROM `servcheck` WHERE act_id = $act_id";
            $res = DB::select($sql);
            if (count($res) > 0) {
                //$list_order_id = array();
                foreach ($res as $val) {
                    //$list_order_id[] = $value->id;
                    self::AddOrderMember($val->id, $value, $role);
                    //если добавили нового члена комиссии в Акт, нужно сбросить итоговую резолюцию для каждого дела в servcheck, если она была выставлена ранее, с предыдущим составом комиссии
                    $data['tbl_name'] = 'servcheck';
                    $data['id'] = $val->id;
                    $data['resolution'] = 0;
                    $data = json_encode($data);
                    self::UpdateAct($data);
                    unset($data);
                };
            };
        };
    }

    public static function UpdateAct($data)
    {
        if (DB::connection()->getDatabaseName()) {
            $fields_values = json_decode($data, true);
            $tbl_name = $fields_values['tbl_name'];
            $id = $fields_values['id'];
            unset($fields_values['tbl_name']);
            unset($fields_values['id']);
            //echo $tbl_name, $id;
            //var_dump($fields_values);

            $query = '';
            foreach ($fields_values as $key => $value) {
                $query = $query . $key . '=' . $value . ', ';
            };
            $query = rtrim($query, ", ");
            $sql = "Update {$tbl_name} set " . $query . " where id = {$id}";
            //echo $sql;
            DB::update($sql);
            self::DBLog($sql);
        } else {
            exit('{"type":"error", "error":"Нет соединения с базой данных"}');
        };
    }

    public static function AddMemberResolution($act_id, $pref_user, $member_resolution, $date_resolution) //простановка в БД резолюции члена комиссии TODO: похоже функция отмерла, проверить
    {
        if (DB::connection()->getDatabaseName()) { //Проверяем подключение к базе
            $sql = "Update acts_member set member_resolution = {$member_resolution} and date_resolution = '{$date_resolution}' where act_id = {$act_id} and pref_user = {$pref_user}";
            //$marker = DB::update($sql);
            if (!DB::update($sql)) {
                exit('{"type":"error", "error":"Не удалось обновить данные"}');
            };
        } else {
            exit('{"type":"error", "error":"Нет соединения с базой данных"}');
        };
    }

    public static function AddMistakeToListOfMistakes($category_id, $mistake_description)
    {
        if (DB::connection()->getDatabaseName()) {
            $sql = "INSERT INTO `pref_mistake`(`category_id`, `description`) VALUES ({$category_id}, '{$mistake_description}')";
            //echo $sql;
            if (DB::Insert($sql)) {
                $id = DB::getPdo()->lastInsertId();
                self::DBLog($sql);
                return $id;
            } else {
                exit('{"type":"error", "error":"Описание ошибки НЕ добавлено в БД"}');
            };
        } else {
            exit('{"type":"error", "error":"Нет соединения с базой данных"}');
        };
    }

    public static function EditMistakeInListOfMistakes($mistake_id, $category_id, $description)
    {
        if (DB::connection()->getDatabaseName()) {
            $data['tbl_name'] = 'pref_mistake';
            $data['id'] = $mistake_id;
            $data['category_id'] = $category_id;
            $data['description'] = "'" . $description . "'";
            $data = json_encode($data);
            self::UpdateAct($data);
            unset($data);
        } else {
            exit('{"type":"error", "error":"Нет соединения с базой данных"}');
        };
    }

    public static function Year() //
    {
        if (DB::connection()->getDatabaseName()) { //Проверяем подключение к базе
            $sql = "Select distinct YEAR(act_date) as year from acts order by act_date desc";
            $array_result = DB::select($sql);
            return json_encode($array_result, JSON_UNESCAPED_UNICODE);
        } else {
            exit('{"type":"error", "error":"Нет соединения с базой данных"}');
        };
    }

    public static function Month() //список месяцев
    {
        $array_result['1'] = 'январь';
        $array_result['2'] = 'февраль';
        $array_result['3'] = 'март';
        $array_result['4'] = 'апрель';
        $array_result['5'] = 'май';
        $array_result['6'] = 'июнь';
        $array_result['7'] = 'июль';
        $array_result['8'] = 'август';
        $array_result['9'] = 'сентябрь';
        $array_result['10'] = 'октябрь';
        $array_result['11'] = 'ноябрь';
        $array_result['12'] = 'декабрь';
        return json_encode($array_result, JSON_UNESCAPED_UNICODE);
    }

    public static function Status() //
    {
        $array_result['null'] = 'Новый';
        $array_result['0'] = 'Завершен';
        $array_result['1'] = 'В работе';
        return json_encode($array_result, JSON_UNESCAPED_UNICODE);
    }

    public static function InfoOfMember($act_id, $role)
    { // получаем ФИО членов комиссии по номеру акта и роли
        if (DB::connection()->getDatabaseName()) { //Проверяем подключение к базе
            switch ($role) {
                case 0:
                    $member_role = 'член комиссии';
                    break;
                case 1:
                    $member_role = 'председатель';
                    break;
                case 2:
                    $member_role = 'секретарь';
                    break;
            };
            $sql = "SELECT `id`,`user_fio_name`, position FROM `acts_member` WHERE `act_id` = {$act_id} and `role` = {$role} order by `user_fio_name`";
            $array_result = DB::select($sql);
            switch (count($array_result)) {
                case 0:
                    $array_res = [];
                    $array_res['type'] = 'error';
                    $array_res['error'] = "Не выбран(ы) {$member_role} комиссии";
                    break;
                case count($array_result) > 1 and $role != 0:
                    $array_res = [];
                    $array_res['type'] = 'error';
                    $array_res['error'] = "{$member_role} указан к кол-ве " . count($array_result) . " шт.";
                    break;
                case count($array_result) > 1 and $role == 0:
                    $array_res = [];
                    $array_res = $array_result;
                    break;
                case 1:
                    $array_res = [];
                    $array_res = $array_result;
                    break;
            };
            return json_encode($array_res, JSON_UNESCAPED_UNICODE);
        } else {
            exit('{"type":"error", "error":"Нет соединения с базой данных"}');
        };
    }

    public static function DBLog($sql)
    { //логирование действий с бд
        $log_date = date("Y-m-d H:i:s");
        $log_user = Core::Init('guest');
        $user = $log_user['user']['firstname'] . ' ' . $log_user['user']['name'] . ' ' . $log_user['user']['surname'];
        $department = $log_user['user']['department'];
        $table_name = '';
        $action = mb_strtolower(substr($sql, 0, strpos($sql, " ")));
        $words = explode(" ", $sql);
        switch ($action) {
            case 'update':
                $table_name = $words[1];
                break;
            case 'insert' or 'delete':
                $table_name = $words[2];
                break;
        };

        $q = "INSERT INTO `pref_db_log` (`log_date`, `log_user`, `department_name`, `tbl`, `action`, `sql_text`) VALUES ('{$log_date}','{$user}','{$department}','{$table_name}','{$action}','" . addslashes($sql) . "')";
        DB::insert($q);
    }

    public static function AddOrderMember($order_id, $member_id, $role)
    { //установление связи между id дела и id member с резолюцией по делу
        $sql = "INSERT INTO `pref_order_member` (`order_id`, `member_id`, `role`) VALUES ({$order_id},{$member_id},{$role})";
        DB::insert($sql);
        self::DBLog($sql);
    }

    public static function PutResolutionResult($order_id)
    { //установка итоговой резолюции по делу, если все члены комиссии выставили свои резолюции

        $res = DB::select("call CheckResolutionResult({$order_id})");
        foreach ($res as $value) {
            $result = $value->res;
        };

        if ($result == 'yes') {
            //echo $result;
            $res = DB::select("call PutResolutionResult({$order_id})");
            return true;
        } else {
            return false;
        };
    }

    public static function SendQueryMail($act_id, $role = '', $user_data = array())
    { //отправить письмо на email членам комиссии для запроса резолюции
        $link = env('APP_URL') . '/servcheck-akt-' . $act_id;
        //echo $role;
        if ($role == 0 or $role == 1 or $role == 2) //  0 1 2 - члены комиссии
        {

            $sql = "SELECT `acts_member`.`id`, `pref_user`, `user_fio_name`, `role`, `member_resolution`, `date_resolution`, `email` FROM `acts_member`, `users-info` where `act_id` = {$act_id} and `acts_member`.`pref_user` = `users-info`.`id`";
            $res = DB::select($sql);
            foreach ($res as $key => $value) {
                $user_fio_name = $value->user_fio_name;
                $email = $value->email;
                $role = $value->role;
                $message = "Уважаемый(-ая) {$user_fio_name}! Вы назначены членом комиссии для проведения проверки по Акту. Просим Вас внести резолюцию по каждому делу в Акте. Для внесения резолюции, перейдите по ссылке:   " . $link;
                EmailController::send($email, 'Запрос резолюции', $message);
            };
        };

        if ($role == 3) //  3 - сотрудник совершивший ошибку
        {
            $message = "Уважаемый(-ая) {$user_data['fio']}! Комиссия установила, что Вы совершили ошибку по делу из Акта. Просим Вас заполнить объяснительную записку, которая необходима для проведения служебной проверки. Убедитесь, что Вы зарегистрированы на портале https://service.mfc-chita.ru/  . Для внесения объяснительной и ознакомлением с делом, перейдите по ссылке:   " . $link;
            EmailController::send($user_data['email'], 'Запрос объяснительной', $message);
        };
    }

    public static function ChangeMemberInMembersList($act_id, $member_id, $new_member_id, $role) //только для председателя и секретаря
    { //изменяем председателя или сектераря в списке членов комиссии
        $sql = "SELECT m.`id` as id, m.`order_id`, m.`member_id`, m.`role`, m.`member_resolution` FROM servcheck, `pref_order_member`as m WHERE m.role = {$role} and m.order_id = servcheck.id and servcheck.act_id = {$act_id}";
        $res = DB::select($sql);
        if (count($res) == 1) {
            $data['tbl_name'] = 'pref_order_member';
            $data['id'] = $res[0]->id;
            $data['member_id'] = $new_member_id;
            $data['member_resolution'] = 'NULL';
            $data['date_resolution'] = 'NULL';
            $data = json_encode($data);
            self::UpdateAct($data);
            unset($data);
        };
        $sql = "SELECT `id`,`act_id`,`pref_user`,`user_fio_name`,`position`,`role` FROM `acts_member` WHERE `act_id` = {$act_id} and `role` = {$role}";
        $res = DB::select($sql);
        if (count($res) == 1) {
            $member = json_decode(self::GetEmployeePosition($new_member_id), true);
            $member_display_name = $member[0]['display_name'];
            $member_position = $member[0]['position'];

            $data['tbl_name'] = 'acts_member';
            $data['id'] = $res[0]->id;
            $data['pref_user'] = $new_member_id;
            $data['user_fio_name'] = "'" . $member_display_name . "'";
            $data['position'] = "'" . $member_position . "'";
            $data = json_encode($data, JSON_UNESCAPED_UNICODE);
            self::UpdateAct($data);
            unset($data);
        };
    }

    public static function DeleteMember($act_id, $member_id, $role)
    {
        $sql = "delete from pref_order_member where id in (
        SELECT pref_order_member.id FROM `pref_order_member`, servcheck WHERE servcheck.act_id = {$act_id} and pref_order_member.order_id = servcheck.id and member_id = {$member_id} and role = {$role})";
        DB::delete($sql);
        self::DBLog($sql);
        $sql = "DELETE FROM `acts_member` WHERE act_id = {$act_id} and pref_user = {$member_id} and role = {$role}";
        DB::delete($sql);
        self::DBLog($sql);
    }

    public static function DelMistakeFromOrder($mistake_id, $order_id)
    { //удалить ошибку из дела

        $sql = "DELETE FROM `pref_order_mistake` WHERE order_id = {$order_id} and mistake_id = {$mistake_id}";
        if (DB::delete($sql)) {
            self::DBLog($sql);
            return true;
        } else {
            return false;
        };
    }

    public static function DeleteOrder($order_id)
    { //удаляем все по делу
        if (DB::connection()->getDatabaseName()) { //Проверяем подключение к базе

            //TODO:: удалить записи о рекомендациях по ошибкам этого дела
            self::DeleteAttachRecommendToOrder($order_id, 0, $kind = 'del_order');

            $sql = "DELETE FROM `pref_order_comment` WHERE order_id = {$order_id}";
            if (DB::delete($sql)) {
                self::DBLog($sql);
            };

            $sql = "DELETE FROM `pref_order_member` WHERE order_id = {$order_id}";
            if (DB::delete($sql)) {
                self::DBLog($sql);
            };

            $sql = "DELETE FROM `pref_order_mistake` WHERE order_id = {$order_id}";
            if (DB::delete($sql)) {
                self::DBLog($sql);
            };

            $sql = "DELETE FROM `servcheck` WHERE id = {$order_id}";
            if (DB::delete($sql)) {
                self::DBLog($sql);
            };
        } else {
            exit('{"type":"error", "error":"Нет соединения с базой данных"}');
        };
    }

    public static function SearchOrder($requesters_fio)
    { //поиск по ФИО или по номеру дела TODO: доделать
        if (strpos($requesters_fio, '/') != false) {
            $query = " and [pref_order].order_number like '{$requesters_fio}%'";
        } else {
            $query = " and [requesters].display_name like '{$requesters_fio}%'";
        };

        $conn = MSSQL::connect();
        $sql = "SELECT top 5
            [pref_order].[id]
            ,[requesters].display_name as requesters_fio
            ,[pref_order].[order_number]
            ,FORMAT([pref_order].[order_date], 'dd.MM.yyyy') as order_date
            , pref_order.pk_pvd_number
            ,pref_mfc.name as mfc_name
            FROM [TEST_ATConsalting].[dbo].[pref_order], [TEST_ATConsalting].[dbo].[pref_department], [TEST_ATConsalting].[dbo].[pref_mfc], [TEST_ATConsalting].[dbo].[pref_user], [TEST_ATConsalting].[dbo].[requesters]
            where
            [pref_order].deleted = 0  AND
            [pref_order].order_type = 'SERVICE' and
            [pref_order].department_id = [pref_department].id and
            [pref_order].mfc = [pref_mfc].id and
            [pref_order].pref_user = [pref_user].id and
            [pref_order].requesters = requesters.id and
            [pref_order].[order_date] >= DATEADD(MONTH, -2, GETDATE())" . $query;

        //echo $sql;
        //--([pref_order].order_number = '1/74/09022023/624602' or)
        //--[requesters].display_name like '{$requesters_fio}%'

        $array_result = MSSQL::select($conn, $sql);
        if (count($array_result) > 0) {
            return json_encode($array_result, JSON_UNESCAPED_UNICODE);
        } else {
            return false;
        };
    }

    public function AddNoteToDB($order_id, $note, $author)
    { //вносим в базу объяснительную сотрудника, пояснительную начальника, выписку из закона от секретаря
        if (DB::connection()->getDatabaseName()) { //Проверяем подключение к базе

            switch ($author) {
                case 'employee':
                    $update_data = array('tbl_name' => 'servcheck', 'employee_note' => $note, 'id' => $order_id);
                    self::UpdateAct(json_encode($update_data));
                    break;
                case 'secretary':
                    $update_data = array('tbl_name' => 'servcheck', 'description' => $note, 'id' => $order_id);
                    self::UpdateAct(json_encode($update_data));
                    break;
                case 'chief':
                    $update_data = array('tbl_name' => 'servcheck', 'comment' => $note, 'id' => $order_id);
                    self::UpdateAct(json_encode($update_data));
                    break;
            };
        } else {
            exit('{"type":"error", "error":"Нет соединения с базой данных"}');
        };
    }

    public function ChangeActStatus($act_id, $status)
    { //меняем статус Акта, например закрываем Акт
        $update_data = array('tbl_name' => 'acts', 'status' => $status, 'id' => $act_id);
        self::UpdateAct(json_encode($update_data));
    }

    public function ChangeThePerson($order_id, $user_id)
    { //Установить пользователя, допустившего ошибку, вместо сотрудника приема

        $user_info = self::GetPosition($user_id);

        $user_fio_name = "'" . $user_info['display_name'] . "'";

        $update_data = array('tbl_name' => 'servcheck', 'pref_user' => $user_id, 'user_fio_name' => $user_fio_name, 'id' => $order_id);
        self::UpdateAct(json_encode($update_data));
    }

    public function GetUUIDMembers($act_id)
    { //логотипы сотрудников для отображения в списке членов комиссии
        $sql = "SELECT `acts_member`.pref_user, `users-info`.`email`, `users-info`.`uuid`, `users-info`.`title`, `users-info`.`department`  FROM `acts_member`, `users-info` WHERE `act_id` = {$act_id} and `acts_member`.`pref_user` = `users-info`.`id`";
        $res = DB::select($sql);
    }

    public static function CountResolutionOfMember($act_id, $member_id)
    { //резолюция по всем делам конкретного члена комиссии с общим ко-вом его резолюций
        if (DB::connection()->getDatabaseName()) { //Проверяем подключение к базе
            $sql = "select x.*, y.cnt_resol from ( SELECT `order_id`,`member_id`,`member_resolution` FROM `pref_order_member` WHERE `order_id` in (SELECT `id` FROM `servcheck` where act_id = {$act_id}) and `member_id` = {$member_id} group by `order_id`,`member_id`,`member_resolution`) x left join ( SELECT `member_id`, count(`member_resolution`) as cnt_resol from `pref_order_member` WHERE `order_id` in (SELECT `id` FROM `servcheck` where act_id = {$act_id}) and `member_id` = {$member_id} group by `member_id` ) y on y.`member_id` = x.`member_id`";
            $res = DB::select($sql);
            return $res;
        } else {
            exit('{"type":"error", "error":"Нет соединения с базой данных"}');
        };
    }

    public static function PutCritical($mistake_id, $critical)
    { //установить/снять фактор критичности

        $data['tbl_name'] = 'pref_order_mistake';
        $data['id'] = $mistake_id;              //id строки pref_order_mistake
        $data['critical'] = $critical;
        $data = json_encode($data);

        if (self::UpdateAct($data)) {
            return 'ok';
        } else {

            if ($critical == 1) {
            //если ошибка критическая, то сразу генерируем Приказ СП по данному факту и отправляем на емайл в ОКА
            //находим номер Акта
                $sql = "SELECT acts.id FROM `acts`, servcheck, pref_order_mistake WHERE pref_order_mistake.id = {$mistake_id} and pref_order_mistake.order_id = servcheck.id and servcheck.act_id = acts.id";
                $res = DB::select($sql);
                if (count($res) == 1) {
                    $act_id = $res[0]->id;
                    self::CreatePrikazAndSendEmail($act_id);
                };
            };

            return 'error';
        };
    }

    public static function CreatePrikazServCheck($act_id)
    { //сгенерировать Приказ о Служебной проверке по факту выявления критической ошибки
        $sql = "SELECT DISTINCT `mfc_name` FROM `servcheck`, pref_order_mistake WHERE act_id = {$act_id} and pref_order_mistake.critical = 1 and pref_order_mistake.order_id = servcheck.id";
        $res = DB::select($sql);

        if (count($res) > 0) {
            $mfc = ''; //список МФЦ в которых совершены ошибки
            foreach ($res as $value) {
                $mfc = $mfc . ', ' . $value->mfc_name;
            };
            $mfc = substr($mfc, 2, strlen($mfc));
            $check_date = date('d.m.Y');     //дедлайн служебной проверки TODO: заменить
            $mistake_date = date('d.m.Y');   //период из письма, за какой месяц ошибки TODO: заменить

            $temp = json_decode(self::InfoOfMember($act_id, 1), true);
            $member_info = (array) $temp[0];

            //если задан председатель в комиссии
            if (array_key_exists('user_fio_name', $member_info)) {

                $president = 'Председатель комиссии: ' . $member_info['user_fio_name'] . ' - ' . $member_info['position'] . ';' . '</w:t></w:r></w:p><w:p w:rsidR="00D11242"><w:r><w:t>';
            } else {
                $president = '';
            };

            $temp = json_decode(self::InfoOfMember($act_id, 2), true);
            $member_info = (array) $temp[0];
            if (array_key_exists('user_fio_name', $member_info)) {
                $secretary = 'Секретарь комиссии: ' . $member_info['user_fio_name'] . ' - ' . $member_info['position'];
            } else {
                $secretary = '';
            };

            $temp = json_decode(self::InfoOfMember($act_id, 0), true);
            $members = '';
            foreach ($temp as $key => $value) {
                $member_info = (array) $temp[$key];
                if (array_key_exists('user_fio_name', $member_info)) {
                    $members = $members . $member_info['user_fio_name'] . ' - ' . $member_info['position'] . '</w:t></w:r></w:p><w:p w:rsidR="00D11242"><w:r><w:t>';
                };
            };

            $commission = $president . $members . $secretary;    //комиссия
            $chief_date = date('d.m.Y'); //дедлайн предоставления объяснительных, пояснительных

            //$path = env('APP_ROOT_DIR') . '/_localdev/template/';
            //$template = $path . 'prikaz_template.docx';
            //$new_prikaz = $path . 'prikaz_' . $act_id . '.docx';

            $template = env('APP_ROOT_DIR') . '/storage/templates/ServCheck/prikaz_template.docx';
            $new_prikaz = env('APP_STORAGE_DIR') . '/ServCheck/prikazTemp/prikaz_' . $act_id . '.docx';

            if (copy($template, $new_prikaz)) {
                //если копия файла создана успешно
                //создаем объект-архив (документ .docx)
                $docx_template = new ZipArchive();

                if ($docx_template->open($new_prikaz)) { //если файл открылся для редактирования
                    $prikaz = $docx_template->getFromName('word/document.xml');
                    //$prikaz_str = strip_tags($prikaz);
                    $prikaz  = str_replace('mistakedate', $mistake_date, $prikaz);
                    //подставляем список МФЦ
                    $prikaz  = str_replace('mfc', $mfc, $prikaz);
                    $prikaz  = str_replace('members', $commission, $prikaz);
                    $prikaz  = str_replace('chiefdate', $chief_date, $prikaz);
                    $prikaz  = str_replace('checkdate', $check_date, $prikaz);
                    $docx_template->addFromString('word/document.xml', $prikaz);
                    $docx_template->close();
                };
            };
            return true;
        } else {
            return false;
        };
    }

    public static function AddRecommendToList($name)
    { //добавление новой методической рекомндации в общий список рекомендаций
        $sql = "SELECT name from pref_recommend where name = '{$name}'";
        if (count(DB::select($sql)) > 0) {
            $sql = "Insert into pref_recommend (name) values ('{$name}')";
            if (DB::insert($sql)) {
                self::DBLog($sql);
                return true;
            } else {
                return false;
            };
        } else {
            return 'error';
        };
    }

    public static function AttachRecommentToOrder($recommend_id, $ord_mis_id)
    { //делаем связь таблиц pref_order_mistake и pref_recommend
        //сначала проверяем связь на дубль
        $sql = "SELECT * FROM `pref_order_recommend` WHERE `ord_mis_id` = {$ord_mis_id} and `recommend_id` = {$recommend_id}";
        if (count(DB::select($sql)) == 0) {
            $sql = "INSERT INTO `pref_order_recommend`(`ord_mis_id`, `recommend_id`) VALUES ({$ord_mis_id},{$recommend_id})";
            if (DB::insert($sql)) {
                return DB::getPdo()->lastInsertId();
            } else {
                return false;
            };
        } else {
            return false;
        };
    }

    public static function GetRecommendFromOrderMistake($order_id, $mistake_id)
    { //получить список рекомендаций для конкретной ошибки по конкретному делу (pref_order_mistake)
        $sql = "SELECT t1.`id`,t1.`ord_mis_id`,t1.`recommend_id`, t2.name FROM `pref_order_recommend` as t1, pref_recommend as t2, pref_order_mistake as t3 where t2.id = t1.recommend_id and t3.order_id = {$order_id} and t3.mistake_id = {$mistake_id} and t3.id = t1.ord_mis_id";
        return DB::select($sql);
    }

    public static function DeleteAttachRecommendToOrder($order_id, $mistake_id, $kind = 'del_recommend')
    {
        $query = " and `mistake_id`= {$mistake_id}";
        $sql = "SELECT `id` FROM `pref_order_mistake` WHERE `order_id` = {$order_id}";
        if ($kind == 'del_recommend') {
            $sql = $sql . $query;
            if (count(DB::select($sql)) == 1) {
                $res = DB::select($sql);
                $id = $res[0]->id;
                $sql = "DELETE FROM `pref_order_recommend` WHERE `ord_mis_id` = {$id}";
                DB::delete($sql);
                self::DBLog($sql);
            };
        } elseif ($kind == 'del_order') {
            if (count(DB::select($sql)) > 0) {
                $res = DB::select($sql);
                foreach ($res as $value) {
                    $id = $value->id;
                    $sql = "DELETE FROM `pref_order_recommend` WHERE `ord_mis_id` = {$id}";
                    DB::delete($sql);
                    self::DBLog($sql);
                };
            };
        };
    }

    public static function GetAllRecommend()
    {
        $sql = "SELECT `id`,`name` FROM `pref_recommend` order by `name`";
        return DB::select($sql);
    }

    public static function GetNameRecommendById($recommend_id)
    {
        $sql = "Select name from pref_recommend where id = {$recommend_id}";
        return DB::select($sql)[0]->name;
    }

    public static function GetFilialsList()
    { //список филиалов из актов
        $sql = "SELECT DISTINCT `mfc_name`, `mfc_id` FROM `servcheck`;";
        $array_result = DB::select($sql);
        return json_encode($array_result, JSON_UNESCAPED_UNICODE);
    }

    public static function GetUsersList()
    { //список виновных сотрудников
        $sql = "SELECT DISTINCT `pref_user`, `user_fio_name` FROM `servcheck`;";
        $array_result = DB::select($sql);
        return json_encode($array_result, JSON_UNESCAPED_UNICODE);
    }

    public static function GetServiceList()
    { //список услуг из всех актов
        $sql = "SELECT DISTINCT `service_eid`, `service_title` FROM `servcheck`;";
        $array_result = DB::select($sql);
        return json_encode($array_result, JSON_UNESCAPED_UNICODE);
    }

    public static function GetMistakesReport($value, $period, $report_type)
    { //отчет по ошибкам
        switch ($report_type) {
            case 'departments':
                $sql = "SELECT IFNULL(COUNT(pref_order_mistake.mistake_id), 0) as mistakes,
                    IFNULL(SUM(pref_order_mistake.critical),0) as critical,
                    (SELECT DISTINCT `name` FROM `departments` WHERE id = {$value} ) as department,
                    (SELECT id FROM `departments` WHERE id = {$value} ) as department_id
                FROM servcheck, pref_order_mistake, acts, departments
                WHERE departments.id = {$value} AND servcheck.department_id = {$value} AND servcheck.id = pref_order_mistake.order_id AND servcheck.act_id = acts.id AND acts.act_date >= '{$period['from']}' AND acts.act_date <= '{$period['to']}';";
                break;

            case 'filials':
                /*$sql = "SELECT IFNULL(COUNT(pref_order_mistake.mistake_id), 0) as mistakes,
                IFNULL(SUM(pref_order_mistake.critical),0) as critical,

                (select distinct mfc_name from servcheck where mfc_id = {$value}) as mfc_name,
                {$value} as mfc_id
                FROM servcheck, pref_order_mistake, acts
                WHERE
                servcheck.mfc_id = {$value} AND
                servcheck.id = pref_order_mistake.order_id AND
                servcheck.act_id = acts.id AND
                acts.act_date >= '{$period['from']}' AND acts.act_date <= '{$period['to']}' ";*/

                $sql = "SELECT IFNULL(COUNT(pref_order_mistake.mistake_id), 0) as mistakes,
                IFNULL(SUM(pref_order_mistake.critical),0) as critical,

                (select distinct mfc_name from servcheck where mfc_id = {$value}) as department,
                {$value} as department_id
                FROM servcheck, pref_order_mistake, acts
                WHERE
                servcheck.mfc_id = {$value} AND
                servcheck.id = pref_order_mistake.order_id AND
                servcheck.act_id = acts.id AND
                acts.act_date >= '{$period['from']}' AND acts.act_date <= '{$period['to']}' ";
                break;

            case 'services':
                /*$sql = "SELECT IFNULL(COUNT(pref_order_mistake.mistake_id), 0) as mistakes,
                    IFNULL(SUM(pref_order_mistake.critical),0) as critical,
                    (select distinct service_title from servcheck where service_eid = '{$value}') as service_title,
                    '{$value}' as service_eid
                    FROM servcheck, pref_order_mistake, acts
                    WHERE servcheck.service_eid = '{$value}' AND
                     servcheck.id = pref_order_mistake.order_id AND
                      servcheck.act_id = acts.id AND
                       acts.act_date >= '{$period['from']}' AND
                        acts.act_date <= '{$period['to']}';
                ";*/
                $sql = "SELECT IFNULL(COUNT(pref_order_mistake.mistake_id), 0) as mistakes,
                    IFNULL(SUM(pref_order_mistake.critical),0) as critical,
                    (select distinct service_title from servcheck where service_eid = '{$value}') as department,
                    '{$value}' as department_id
                    FROM servcheck, pref_order_mistake, acts
                    WHERE servcheck.service_eid = '{$value}' AND
                     servcheck.id = pref_order_mistake.order_id AND
                      servcheck.act_id = acts.id AND
                       acts.act_date >= '{$period['from']}' AND
                        acts.act_date <= '{$period['to']}';
                ";
                break;
            case 'users':
                /*$sql = "SELECT IFNULL(COUNT(pref_order_mistake.mistake_id), 0) as mistakes,
                    IFNULL(SUM(pref_order_mistake.critical),0) as critical,
                    (select distinct user_fio_name from servcheck where pref_user = '{$value}') as user_fio_name,
                    '{$value}' as pref_user
                    FROM servcheck, pref_order_mistake, acts
                    WHERE servcheck.pref_user = '{$value}' AND
                     servcheck.id = pref_order_mistake.order_id AND
                      servcheck.act_id = acts.id AND
                       acts.act_date >= '{$period['from']}' AND
                        acts.act_date <= '{$period['to']}';
                ";*/
                $sql = "SELECT IFNULL(COUNT(pref_order_mistake.mistake_id), 0) as mistakes,
                    IFNULL(SUM(pref_order_mistake.critical),0) as critical,
                    (select distinct user_fio_name from servcheck where pref_user = '{$value}') as department,
                    '{$value}' as depatrment_id
                    FROM servcheck, pref_order_mistake, acts
                    WHERE servcheck.pref_user = '{$value}' AND
                     servcheck.id = pref_order_mistake.order_id AND
                      servcheck.act_id = acts.id AND
                       acts.act_date >= '{$period['from']}' AND
                        acts.act_date <= '{$period['to']}';
                ";
                break;
        };

        $array_result = DB::select($sql);

        return $array_result;
    }

    public static function CreatePrikazAndSendEmail($act_id)
    {
        if (self::CreatePrikazServCheck($act_id)) {
                    //если файл приказа сгенерирован и есть критические ошибки
                    //ссылка для скачивания
                    $link = env('APP_URL') . '/api/v1_0/ServCheck?type=download_prikaz&act_id=' . $act_id;
                    $email = 'Semchenkova.Irina@mfc-chita.ru'; //TODO: заменить на емайл отдела ОКА
                    //$email = 'Tregub.Darya@mfc-chita.ru';
                    //$email = 'gudkov@mfc-chita.ru';
                    EmailController::send($email, 'Test Рассылка Приказ', 'Скачайте Приказ о СП, перейдя по ссылке: ' . $link, 0, 0, 0);
        };
    }

    public function DataShow(Request $request)
    {
        if ($request->type == 'action') {
            if (isset($request->department_id) && $request->department_id != '' && isset($request->action_id) && $request->action_id != '') {
                $department_id = $request->department_id;
                $this->department_id = $department_id;
                $action_id = $request->action_id;
                //$this->department_id = $request->department_id;
                //echo $this->department_id;
                switch ($action_id) {
                    case 1:                 //создать
                        //$result_array = self::FindMember('front');//члены из MSSQL
                        $result_array = self::FindServiceMember('front'); //члены из MSSQL
                        return $result_array;
                        break;
                    case 2:                 //редактировать
                        $result_array['department_id'] = $department_id;
                        $result_array['action'] = "edit_act";
                        break;
                    case 3:                 //открыть существующий
                        $result_array['department_id'] = $department_id;
                        $result_array['action'] = "open_act";
                        break;
                };
                //print_r($result_array);
                //echo json_encode($result_array, JSON_UNESCAPED_UNICODE);
            };
        };

        if ($request->type == 'selected_members') {

            //file_put_contents('c:\Users\Semchenkova\Documents\1.txt', $postData[1]);

            if (isset($request->members)) {
                $postData = json_decode($request->members, true);
                $str_members = implode(", ", $postData);
                $str_check = str_replace(', ', "", $str_members); //если пользователь кнопку нажал, а членов не выбрал
                if (strlen($str_check) > 0) {
                    $arr_members = explode(", ", $str_members);
                    $head_data_act['members'] = $str_members;
                    $head_data_act['arr_members'] = $arr_members;
                    $head_data_act['department_id'] = 71; // TODO: $department_id;
                    $act_id = self::AddNewAct($head_data_act);
                    return json_encode($act_id, JSON_UNESCAPED_UNICODE);
                };
            } else {
                return '{"type":"error", "error":"Members is not accepted"}';
            };

            //file_put_contents('c:\Users\Semchenkova\Documents\1.txt', $str_members);
        };

        if ($request->type == 'selected_order') //изменить состав выходной инфы
        {
            if (isset($request->order_id) && $request->order_id != '') {
                $order_id = $request->order_id;
                //$department_id = $request->department_id;
                /*$res = [];
                $res['order_data'] = self::GetOrderInfo($order_id);
                //$categories = self::ListOfCategories($department_id);
                $res['categories'] = self::ListOfCategories($department_id);
                return $res;*/
                return self::GetOrderInfo($order_id);
            };
        };

        if ($request->type == 'add_mistaketolist') {
            //добавим новую ошибку(внесем в список ошибок) и сразу же привяжем ее к делу
            if (isset($request->mistake_data)) {

                $postData = json_decode($request->mistake_data, true);
                $category_id = $postData['category_id'];
                $description = $postData['description'];
                $order_id = $postData['order_id'];

                $mistake_id = self::AddMistakeToListOfMistakes($category_id, $description);
                if (self::AddMistakeToOrder($order_id, $mistake_id)) {
                    //возвращаем все ошибки по делу
                    //return json_encode(self::FindOrderMistakes($order_id), JSON_UNESCAPED_UNICODE);
                    return self::FindOrderMistakes($order_id);
                } else {
                    return '{"type":"error", "error":"Ошибка НЕ добавлена. Обратитесь к разработчикам"}';
                };
            };
        };

        if ($request->type == 'edit_mistakeinlist') {
            //редактирование ошибки в списке ошибок
            if (isset($request->mistake_data)) {
                $postData = json_decode($request->mistake_data, true);
                $category_id = $postData['category_id'];
                $description = $postData['description'];
                $mistake_id = $postData['mistake_id'];
                $order_id = $postData['order_id'];
                self::EditMistakeInListOfMistakes($mistake_id, $category_id, $description);
                return self::FindOrderMistakes($order_id);
                //return  '{"type":"ok", "text":"ok"}';
            } else {
                return '{"type":"error", "error":"Ошибка НЕ изменена. Обратитесь к разработчикам"}';
            };
        };

        if ($request->type == 'order_number') {

            if (isset($request->order_number) && $request->order_number != '' && isset($request->department_id) && $request->department_id != '') {

                $order_data = self::GetDataFromMSSQL($request->order_number);

                $categories = self::ListOfCategories($request->department_id);

                //$order_data['comment'] = self::GetComment($request->order_number);
                return json_encode($order_data, JSON_UNESCAPED_UNICODE);
            };
        };

        if ($request->type == 'acts') {

            if (isset($request->department_id)) {
                $department_id = $request->department_id;
            } else {
                $department_id = '';
            };
            if (isset($request->month)) {
                $month = $request->month;
            } else {
                $month = '';
            };
            if (isset($request->year)) {
                $year = $request->year;
            } else {
                $year = '';
            };
            if (isset($request->status)) {
                $status = $request->status;
            } else {
                $status = '';
            };

            //return json_encode(self::GetActs($department_id, $month, $year, $status),JSON_UNESCAPED_UNICODE);
            //return json_encode(self::GetActs(71,'','2022',''),JSON_UNESCAPED_UNICODE);
            return self::GetActs($department_id, $month, $year, $status);
        }

        if ($request->type == 'addact') {
            if (isset($request->department_id) && $request->department_id != '') {
                $department_id = $request->department_id;
                //$str_members = '1, 2, 3';//закоментировать в продакшене
                //$arr_members = explode(", ", $str_members);////закоментировать в продакшене
                //$head_data_act['members'] = $str_members;////закоментировать в продакшене
                //$head_data_act['arr_members'] = $arr_members;////закоментировать в продакшене
                $head_data_act['department_id'] = $department_id;
                $act_id = self::AddNewAct($head_data_act);
                return json_encode('ok', JSON_UNESCAPED_UNICODE);
            }
        }

        if ($request->type == 'selected_act') {
            if (isset($request->act_id) && $request->act_id != '') {
                $act_id = $request->act_id;
                return self::GetActInfo($act_id);
            }
        }

        /*if ($request->type == 'get_category') {
            if (isset($request->category_id) && $request->category_id != '') {
                $category_id = $request->category_id;
                $mistakes = ServCheckController::ListOfMistakes(71, $category_id);
                //$mistakes = ServCheckController::ListOfCategories(71);
                /*echo "<pre>";
                print_r($mistakes);
                echo "</pre>";*/
        //var_dump($mistakes);
        //echo $mistakes;

        //};
        //}; */

        if ($request->type == 'get_mistakes') { //получить список ошибок
            if (isset($request->category_id) && $request->category_id != '') {
                //$mistakes = self::ListOfMistakes($request->category_id);
                return self::ListOfMistakes($request->category_id);
                //echo json_encode($mistakes, JSON_UNESCAPED_UNICODE);
            };
        };

        if ($request->type == 'dropnewact') {
            if (isset($request->act_id) && $request->act_id != '') {
                if (self::DropAct($request->act_id, 'fullact')) {
                    return  '{"type":"ok", "text":"ok"}';
                }; //TODO: проверить, что Акт в статусе Новый и нет дел в нем
            };
        };

        if ($request->type == 'dropfullact') {
            //этот метод будет вызыватся из админской части или использоваться при отладке
            if (isset($request->act_id) && $request->act_id != '') {
                if (self::DropAct($request->act_id, 'fullact')) {
                    return '{"type":"ok", "text":"ok"}';
                };
            };
        };

        if ($request->type == 'addneworder') {
            if (isset($request->act_id) && $request->act_id != '' and isset($request->order_number) && $request->order_number != '') {
                $act_id = $request->act_id;
                $order_number = $request->order_number;

                $data_array = self::GetDataFromMSSQL($order_number, 'back');
                if (isset($data_array)) {
                    $data_array[0]['act_id'] = $act_id;
                    $data_array[0]['description'] = ''; //TODO::Проверить не затираются ли значения в бд пустыми
                    $data_array[0]['comment'] = '';
                    $data_array[0]['employee_note'] = '';
                    $new_order_id = self::PutDataToMySQL($data_array);
                    if ($new_order_id != 0) {
                        self::GetComment($order_number, $new_order_id); //перенесли комментарии сотрудника приема из MSSQL в pref_order_comment
                        //если дело добавлено в Акт, меняем ему статус
                        $update_data = array('tbl_name' => 'acts', 'status' => 1, 'id' => $act_id);

                        self::UpdateAct(json_encode($update_data));
                        // список дел:
                        return self::GetOrdersTable($act_id);
                        //echo '{"type":"ok", "order_id":'.$new_order_id.'}';
                    } else {
                        echo '{"type":"error", "error":"Данные не добавлены"}';
                    };
                };
            };
        };

        if ($request->type == 'del_order') {
            //удалить дело, почистить сопутствующие таблицы
            if (isset($request->act_id) && $request->act_id != '' and isset($request->order_id) && $request->order_id != '') {
                $act_id = $request->act_id;
                $order_id = $request->order_id;
                self::DeleteOrder($order_id);
                $sql = "SELECT `id` FROM `servcheck` where `act_id` = {$act_id}";
                if (count(DB::select($sql)) == 0) {
                    //если удалено последнее дело из Акта, меняем статун на Новый
                    $update_data = array('tbl_name' => 'acts', 'status' => 'null', 'id' => $act_id);
                    self::UpdateAct(json_encode($update_data));
                };
                return self::GetOrdersTable($act_id);
            };
        };

        if ($request->type == 'infomember') {
            if (isset($request->act_id) && $request->act_id != '' && isset($request->role) && $request->role != '') {
                echo self::InfoOfMember($request->act_id, $request->role);
            };
        };

        if ($request->type == 'update_order') {
        };

        // if ($request->type == 'year') {
        //     return self::Year();
        // }

        // if ($request->type == 'month') {
        //     if (isset($request->year) && $request->year != '') {
        //         $year = $request->year;
        //         return self::Month($year);
        //     };
        // }

        if ($request->type == 'list_members') { //вносим изменения в состав комиссии
            if (isset($request->members)) {

                $membersData = $request->members;
                $act_id = $membersData['act_id'];
                $president = $membersData['role_1']; //тип string
                $secretary = $membersData['role_2']; //тип string

                if (isset($membersData['role_0'])) {
                    $members = $membersData['role_0'];
                } else {
                    $members = [];
                }

                $ActMembersFromDB = self::GetMembersFromAct($act_id); //получаем всех членов из акта

                //изменяем членов комиссии, сравниваем два набора значений
                if (!empty($members)) { //если есть хоть один член комиссии в списке пришедшем с фронта
                    $arr_dubl = array_intersect($ActMembersFromDB['member'], $members);
                    $add_members = array_diff($members, $arr_dubl); //этих нужно добавить
                    $del_members = array_diff($ActMembersFromDB['member'], $arr_dubl); //этих удалить

                    foreach ($del_members as $value) {
                        self::DeleteMember($act_id, $value, 0);
                        //перепроверим итоговую резолюцию
                        //self::PutResolutionResult($request->order_id); //TODO: нужно найти все дела по которым он выставил резолюции и вычистить
                    };

                    //добавляем членов комиссии в акт
                    if (!empty($add_members)) {
                        $head_data_act['arr_members'] = $add_members;
                        self::AddMembersToAct($act_id, $head_data_act);
                    };
                } else { //если с фронта пришел пустой массив членов - удалим всех членов из Акта

                    $act_members = $ActMembersFromDB['member'];
                    //var_dump($act_members);
                    foreach ($act_members as $value) {
                        //echo $value;
                        self::DeleteMember($act_id, $value, 0);
                        //перепроверим итоговую резолюцию
                        //self::PutResolutionResult($request->order_id); //TODO: нужно найти все дела по которым он выставил резолюции и вычистить
                    };
                };

                //удалим председателя в акте, если с фронта пришло пустое значение
                if ($president == '' && !empty($ActMembersFromDB['president'])) {
                    self::DeleteMember($act_id, $ActMembersFromDB['president'][0], 1);
                };

                //удалим секретаря в акте, если с фронта пришло пустое значение
                if ($secretary == '' && !empty($ActMembersFromDB['secretary'])) {
                    self::DeleteMember($act_id, $ActMembersFromDB['secretary'][0], 2);
                };

                //меняем председателя в акте, если новый отличается от того, что в БД
                if (!empty($ActMembersFromDB['president']) and $president != '' and ($ActMembersFromDB['president'] != $president)) {
                    self::ChangeMemberInMembersList($act_id, $ActMembersFromDB['president'], $president, 1);
                };
                //меняем секретаря в акте
                if (!empty($ActMembersFromDB['secretary']) and $secretary != '' and ($ActMembersFromDB['secretary'] != $secretary)) {
                    self::ChangeMemberInMembersList($act_id, $ActMembersFromDB['secretary'], $secretary, 2);
                };
                //добавляем в акт председателя, если его не было
                if (empty($ActMembersFromDB['president']) and $president != '') {
                    $str_members = $president; //добавляем члена к акту
                    $str_roles = '1';
                    $arr_members = explode(", ", $str_members);
                    $arr_roles = explode(", ", $str_roles);
                    $head_data_act['arr_members'] = $arr_members;
                    $head_data_act['arr_roles'] = $arr_roles;

                    self::AddMembersToAct($act_id, $head_data_act);
                };
                //добавляем в акт секретаря, если его не было
                if (empty($ActMembersFromDB['secretary']) and $secretary != '') {

                    $str_members = $secretary; //добавляем члена к акту
                    $str_roles = '2';
                    $arr_members = explode(", ", $str_members);
                    $arr_roles = explode(", ", $str_roles);
                    $head_data_act['arr_members'] = $arr_members;
                    $head_data_act['arr_roles'] = $arr_roles;

                    self::AddMembersToAct($act_id, $head_data_act);
                };


                //return '{"type":"ok", "text":"ok"}';//так было до 18.05.23
                return self::ArrayResolutionOfMembers($act_id);
            } else {
                return '{"type":"error", "error":"Members is not accepted"}';
            };
        };

        if ($request->type == 'add_mistake_order') {
            //ошибку, которая уже есть в списке ошибок, привязываем к делу
            if (isset($request->mistake_id) && $request->mistake_id != '' && isset($request->order_id) && $request->order_id != '') {
                $order_id = $request->order_id;
                $mistake_id = $request->mistake_id;
                //проверяем на повтор перед добавлением
                $sql = "SELECT id FROM `pref_order_mistake` WHERE `order_id` = {$order_id} and `mistake_id`= {$mistake_id}";
                if (count(DB::select($sql)) > 0) {
                    return '{"type":"error", "error":"Такая ошибка уже есть в деле"}';
                } else {
                    if (self::AddMistakeToOrder($order_id, $mistake_id)) {
                        //возвращаем все ошибки по делу
                        return json_encode(self::FindOrderMistakes($order_id), JSON_UNESCAPED_UNICODE);
                        //return self::FindOrderMistakes($order_id);
                    } else {
                        return '{"type":"error", "error":"Ошибка НЕ добавлена. Обратитесь к разработчикам"}';
                    };
                };
            };
        };

        if ($request->type == 'del_mistake_order') {
            if (isset($request->mistake_id) && $request->mistake_id != '' && isset($request->order_id) && $request->order_id != '') {
                self::DeleteAttachRecommendToOrder($request->order_id, $request->mistake_id);
                if (self::DelMistakeFromOrder($request->mistake_id, $request->order_id)) {
                    return json_encode(self::FindOrderMistakes($request->order_id), JSON_UNESCAPED_UNICODE);
                    //return self::FindOrderMistakes($order_id);
                } else {
                    return '{"type":"error", "error":"Ошибка НЕ удалена. Обратитесь к разработчикам"}';
                };
            };
        };

        if ($request->type == 'send_mail') {
            //запросить резолюцию, пояснительную, объяснительную
            if (isset($request->act_id) && $request->act_id != '') {

                $act_id = $request->act_id;
                $user = Core::Init('guest');
                $user_id = $user['user']['id']; //id пользователя которому отправляем письмо
                $user_fio = $user['user']['firstname'] . ' ' . $user['user']['name'] . ' ' . $user['user']['surname'];

                //проверим, является ли текущий пользователь членом комиссии для данного акта
                $sql = "SELECT `acts_member`.`id`, `pref_user`, `user_fio_name`, `role`, `member_resolution`, `date_resolution`, `email` FROM `acts_member`, `users-info` where `act_id` = {$act_id} and `acts_member`.`pref_user` = `users-info`.`id` and `pref_user` = {$user_id}";

                //echo $sql;

                if (count(DB::select($sql)) == 1) { //пользователь - член комиссии
                    self::SendQueryMail($act_id, 1);
                    return  '{"type":"ok", "text":"Запрос резолюции отправлен"}';
                } else {
                    //проверим, что текущий пользователь - сотрудник, допустивший ошибку
                    $sql = "SELECT servcheck.`id`, servcheck.`pref_user`, servcheck.`user_fio_name`, `employee_note`, `comment`, `mfc_id`, CONCAT(`users-info`.firstname,' ', `users-info`.name,' ', `users-info`.surname) as fio, `users-info`.email FROM `servcheck`, `users-info` WHERE servcheck.`act_id` = {$act_id} and servcheck.`user_fio_name` = '{$user_fio}' and servcheck.`user_fio_name` = CONCAT(`users-info`.firstname,' ', `users-info`.name,' ', `users-info`.surname);"; //TODO::исправить запрос
                    if (count(DB::select($sql)) == 1) {
                        //$res = DB::select($sql);
                        $res = (array) DB::select($sql)[0];
                        if ($res['email']) {
                            //echo $res['email'];
                            self::SendQueryMail($act_id, 3, $res);
                            return  '{"type":"ok", "text":"Отправлен запрос объясниельной на "' . $res['email'] . '}';
                        } else {
                            return 'нет email у сотрудника';
                        };
                    };
                };

                /*$user_id = $request->user_id;
                $act_id = $request->act_id;
                $sql = "SELECT `role` FROM `acts_member` WHERE `pref_user` = {$user_id} and `act_id` = {$act_id}";
                $res = DB::select($sql);
                if (count($res) == 1) {
                    $role = $res[0]->role;
                    self::SendQueryMail($user_id, $act_id, $role);
                    return '{"type":"ok", "text":"ok"}';
                }
                else {return '{"type":"error", "text":"Пользователь не является членом комиссии по данному Акту. Резолюция не запрошена"}';};*/
            };
        };

        if ($request->type == 'add_critical') {
            //меняем статус ошибки критичная/не критичная сразу для массива ошибок
            if (isset($request->mistake_critical) && isset($request->order_id)) {
                $order_id = $request->order_id;
                $postData = json_decode($request->mistake_critical, true);
                //var_dump($postData);
                $result_array = array();
                foreach ($postData as $key => $value) {
                    //echo $value;
                    //echo self::PutCritical($key, $value);
                    if (self::PutCritical($key, $value) == 'error') {
                        $result_array = self::FindOrderMistakes($order_id);
                    } else {
                        $result_array[$key] = 'error';
                    };
                };
                return $result_array;
            };
        };

        if ($request->type == 'add_resolution') {
            //выставляем резолюцию по делу и делаем запрос на подсчет итоговой резолюции
            if (isset($request->order_id) && $request->order_id != '' && isset($request->member_resolution) && $request->member_resolution != '') {

                $user_data = Core::Init('guest'); //получаем id авторизованного пользователя TODO: решить вопрос с ролями!
                $user_id = $user_data['user']['id'];
                //ищем id записи для запроса update
                $sql = "SELECT `id` FROM `pref_order_member` WHERE `order_id`={$request->order_id} and `member_id`= {$user_id}";
                //echo $sql;
                if (count(DB::select($sql)) == 1) {
                    $res = DB::select($sql);
                    //var_dump($res);
                    $data['tbl_name'] = 'pref_order_member';
                    $data['member_resolution'] = $request->member_resolution;
                    $data['date_resolution'] = "'" . date('Y-m-d H:i:s') . "'";
                    $data['id'] = $res[0]->id;
                    $data = json_encode($data);
                    self::UpdateAct($data);
                    //self::PutResolutionResult($request->order_id);
                    if (self::PutResolutionResult($request->order_id)) {
                        return '{"type":"ok", "text":"Итоговая резолюция установлена"}';
                    } else {
                        return '{"type":"ok", "text":"Итоговая резолюция НЕ установлена"}';
                    };
                } else {
                    return '{"type":"error", "text":"Не найден номер записи для выставления резолюции"}';
                };
            };
        };

        if ($request->type == 'search') {
            if (isset($request->requesters_fio) && $request->requesters_fio != '') {
                $requesters_fio = $request->requesters_fio;
                $res = self::SearchOrder($requesters_fio);
                //var_dump($res);
                if (!empty($res)) {
                    return $res;
                } else {
                    return '{"type":"error", "text":"Не найдены данные"}';
                };
            };
        };

        if ($request->type == 'search_guilty') {
            //поиск нового виновного по ФИО
            if (isset($request->guilty_fio) && $request->guilty_fio != '') {
                $guilty_fio = $request->guilty_fio;
                $res = self::FindMember($letter = $guilty_fio, $kind = 'front');
                if (!empty($res)) {
                    return $res;
                } else {
                    return '{"type":"error", "text":"Не найдены данные"}';
                };
            };
        };

        if ($request->type == 'change_guilty') {
            //заменить id and fio виноватого сотрудника
            if (isset($request->order_id) && $request->order_id != '' && isset($request->user_id) && $request->user_id != '') {
                self::ChangeThePerson($request->order_id, $request->user_id);
                return  '{"type":"ok", "text":"ok"}';
            };
        };

        if ($request->type == 'add_note') {

            if (isset($request->author) && $request->author != '' && isset($request->order_id) && $request->order_id != '') {
                $author = $request->author;
                $order_id = $request->order_id;
                $note = "'" . $request->note . "'";
                self::AddNoteToDB($order_id, $note, $author);
                switch ($author) {
                    case 'secretary':
                        $field = 'description';
                        break;
                    case 'employee':
                        $field = 'employee_note';
                        break;
                    case 'chief':
                        $field = 'comment';
                        break;
                };
                $sql = "Select {$field} from servcheck where id = {$order_id}";
                $res = DB::select($sql);
                return nl2br($res[0]->$field);
            };
        };

        if ($request->type == 'change_status') {
            //self::ChangeActStatus($act_id, $status);
        };

        if ($request->type == 'close_act') {
            if (isset($request->act_id) && $request->act_id != '') {
                $act_id = $request->act_id;

                $res = DB::select("call CheckCloseAct({$act_id})");
                foreach ($res as $value) {  //TODO: подумать как избавиться от цикла
                    $result = $value->res;
                };
                if ($result == 'yes') {
                    self::ChangeActStatus($act_id, 0);
                    return '{"type":"ok", "text":"Акт закрыт"}';
                } else {
                    //TODO: решить нужно ли отправлять фамилии тех, кто не выставил свои резолюции, т.к. они могут в делах повторяться
                    return '{"type":"error", "text":"Не выставлена резолюция членов комиссии"}';
                };
            };
            //self::ChangeActStatus($act_id, $status);
        };

        if ($request->type == 'create_prikaz_sc') {
            if (isset($request->act_id) && $request->act_id != '') {
                self::CreatePrikazAndSendEmail($request->act_id);
            };
        };

        if ($request->type == 'download_prikaz') {
            //скачиваем файл приказа на комп

            //$file = env('APP_ROOT_DIR') . '\_localdev\template\prikaz_' . $request->act_id . '.docx';
            //return Response::download($file, 'prikaz_' . $request->act_id . '.docx');

            return Storage::download('/ServCheck/prikazTemp/prikaz_' . $request->act_id . '.docx');
        };

        if ($request->type == 'add_recommend') {
            //добавление рекомендации в общий список тбл pref_recommend
            if (isset($request->name)) {
                $recommend_name = json_decode($request->name, true);
                if (self::AddRecommendToList($recommend_name)) {
                    return '{"type":"ok", "text":"Метод.рекомендация добавлена в список рекомендаций"}';
                };
            }
        };

        if ($request->type == 'attach_recommend') {
            //привяжем рекомендации к делу и к ошибке по этому делу

            if (isset($request->recommend)) {
                $recommend = [];
                $postData = json_decode($request->recommend, true);
                if (!empty($postData)) {
                    //сначала удалим все рекомендации, которые есть

                    $del_recommend = (array) $postData[0];

                    $sql = "Delete from pref_order_recommend where ord_mis_id = {$del_recommend['ord_mis_id']}";
                    DB::delete($sql);
                    self::DBLog($sql);

                    foreach ($postData as $value) {

                        $recommend_id = $value['recommend_id'];
                        $ord_mis_id = $value['ord_mis_id'];
                        //получаем id новой записи
                        $ord_rec_id = self::AttachRecommentToOrder($recommend_id, $ord_mis_id);
                        if ($ord_rec_id !== false) {
                            $result_array = array();
                            $result_array['id'] = $ord_rec_id;
                            $result_array['ord_mis_id'] = $ord_mis_id;
                            $result_array['recommend_id'] = $recommend_id;
                            $result_array['name'] = self::GetNameRecommendById($recommend_id);
                            $recommend[] = $result_array;
                        };
                    };
                };
                return $recommend;
            } else {
                return "Рекомендации не привязаны";
            };
        };

        if ($request->type == 'get_recommend_from_order_mistake') {
            //перечень рекомендаций по делу и конкретной ошибке
            if (isset($request->order_id) && $request->order_id != '' && isset($request->mistake_id) && $request->mistake_id != '') {
                $order_id = $request->order_id;
                $mistake_id = $request->mistake_id;
                return self::GetRecommendFromOrderMistake($order_id, $mistake_id);
            };
        };

        if ($request->type == 'get_all_recommend') {
            //перечень рекомендаций
            return json_encode(self::GetAllRecommend(), JSON_UNESCAPED_UNICODE);
        };

        if ($request->type == 'get_directors') {


            return json_encode(Core::LDAP_get_directors('for_servcheck'), JSON_UNESCAPED_UNICODE);
        };

        if ($request->type == 'try') {


        };

        if ($request->type == 'get_statistic_filters_info') {
            $result_array = array();
            $result_array['departments'] = self::ListOfDepartments();
            $result_array['filials'] = self::GetFilialsList();
            $result_array['users'] = self::GetUsersList();
            $result_array['services'] = self::GetServiceList();
            return json_encode($result_array, JSON_UNESCAPED_UNICODE);
            // ListOfDepartments
        };

        if ($request->type == 'generate_report') {
            $result = array(); //массив со статистическими данными всеми
            $result_array = array(); //массив со статистическими данными по одному ведомству
            //тип отчета, период и данные для поиска
            if (isset($request->period) && $request->period != '' && isset($request->report) && $request->report != '') {
                $postData = array();
                $period = array();
                $period['from'] = substr($request->period, 6, 4) . '-' . substr($request->period, 3, 2) . '-' . substr($request->period, 0, 2);
                $period['to'] = substr($request->period, 17, 4) . '-' . substr($request->period, 14, 2) . '-' . substr($request->period, 11, 2);
                $postData['report_data'] = json_decode($request->report_data, true);//данные для отчета
                //$postData['report'] = $request->report;//тип отчета
                $report_type = $request->report;

                foreach ($postData['report_data'] as $value) {
                    $result_array = self::GetMistakesReport($value, $period, $report_type);
                    if (!empty($result_array)) {
                        $result[] = $result_array[0];
                    };
                };

                if (!empty($result)) {

                    return $result;
                } else {
                    return '{"type":"error", "text":"Данные не найдены"}';
                };
            } else {
                return '{"type":"error", "text":"Данные не найдены"}';
            };
        };
    }

    public function MySQLTableToJsonArray($table_name) //это проба
    {
        if (DB::connection()->getDatabaseName()) {
            $sql = "select * from {$table_name}";
            $array_result = DB::select($sql);
            echo json_encode($array_result, JSON_UNESCAPED_UNICODE);
        } else {
            exit('{"type":"error", "error":"Нет соединения с базой данных"}');
        };
    }

    public static function FillDataToDB()
    {
        $conn = MSSQL::connect();
        $sql = "
            SELECT
                top 10
                [pref_order].[order_number]
            FROM [TEST_ATConsalting].[dbo].[pref_order]
            where [pref_order].deleted = 0  AND
            [pref_order].order_type = 'SERVICE' and
            [pref_order].department_id = 121 and
            [pref_order].[order_number] not like '%el%'
            order by [order_date]";

        $array_result = MSSQL::select($conn, $sql);
        foreach ($array_result as $value) {
            $order_number = $value['order_number'];
            echo $order_number;
            $data_array = self::GetDataFromMSSQL($order_number, 'back');
            if (isset($data_array)) {
                $data_array[0]['act_id'] = rand(1, 10);
                $data_array[0]['description'] = '';
                $data_array[0]['comment'] = '';
                $data_array[0]['employee_note'] = '';
                $new_order_id = self::PutDataToMySQL($data_array);
            };
        };
    }

    public function test2()
    {
        $sql = "SELECT * FROM `users-info` WHERE `roles` LIKE '%" . '"pref_predsedatel"' . "%'";
        var_dump(DB::select($sql));
    }
}